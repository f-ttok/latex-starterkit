# README #

はじめてLaTeXをさわる人のためのガイドのようなもの

注意：

- 端末という言葉がでてきたら以下のように読み替えてください
    - Windows → コマンドプロンプト
    - Mac → iTerm2 または ターミナル
- ホームフォルダという言葉が出てきたら(ry
    - Windows → コマンドプロンプトで`echo %HOMEPATH%`と打って出てくるフォルダ
    - Mac/Linux → 端末で`echo $HOME`と打って出てくるフォルダ

## 必要なもの
- [TeX Live](https://www.tug.org/texlive/) （2020以降）
    - 基本的には以下の手順でインストールできる
        - How to acquire TeX Liveの隣のdownloadをクリック
        - Windows
            - install-tl-windows.exeをクリックしてダウンロード
            - ダウンロードしたinstall-tl-windows.exeを実行
            - 詳しい手順は[Windowsへのインストール](https://texwiki.texjp.org/?TeX%20Live%2FWindows)を参照
        - Linux/Mac
            - install-tl-unx.tar.gzをクリックしてダウンロード
            - ダウンロードしたinstall-tl-unx.tar.gzを解凍
            - 中を見ていくと`install-tl`というファイルがあるので，そこで端末を開いて`sudo bash ./install-tl`を実行
            - 詳しい手順は[Linuxへのインストール](https://texwiki.texjp.org/?Linux#texlive)及び[Macへのインストール](https://texwiki.texjp.org/?TeX%20Live%2FMac#texlive-install-official)を参照
    - ホームフォルダに`.latexmkrc`というファイルを作って[このページ](https://texwiki.texjp.org/?Latexmk#ke005cd5)の`#!/usr/bin/env/ perl`から`$pdf_previewer = 'xdg-open';}}`までの内容を貼り付ける
        - Windows
            - 一度[VS Code](https://code.visualstudio.com)をインストールする
            - VS Codeのインストール後にコマンドプロンプトで`type nul > %HOMEPATH%\.latexmkrc`と打つ
            - コマンドプロンプトで`code %HOMEPATH%\.latexmkrc`と打ち，VS Codeで`.latexmkrc`を開いたあと，内容をコピペして保存する (<kbd>Ctrl</kbd> + <kbd>S</kbd>).
        - Linux/Mac
            - 一度[VS Code](https://code.visualstudio.com)をインストールする
            - VS Codeのインストール後に端末で`touch $HOME/.latexmkrc`と打つ
            - 端末で`code $HOME/.latexmkrc`と打ち，VS Codeで`.latexmkrc`を開いたあと，内容をコピペして保存する Ctrl + S (Linux) / Cmd + S (Mac).
            - 27行目あたりの`$bibtex = 'upbibtex %O %B';`を`$bibtex = 'pbibtex %O %B';`と修正する
    - 端末を開いて`latex --version`と打ち込み，`pdfTeX 3.14<数字が続く> (TeX Live 20xx)`と出てくればOK．

- [VS Code](https://code.visualstudio.com)
    - [LaTeX Workshop](https://marketplace.visualstudio.com/items?itemName=James-Yu.latex-workshop)という拡張機能をインストールする
    - VS Codeの左側に四角が4つ書かれたアイコンがあるのでそこをクリックして拡張機能インストールメニューを開く
    - latex workshopと打ち込み，該当する拡張機能のインストールボタンを押してインストールする
    - LaTeX Workshopの設定をする
        - VS Codeで Ctrl + , (Windows/Linux) / Cmd + , (Mac) を押してVS Codeの設定画面を開く
        - 設定画面の上の方に検索窓があるので
            - "latex workshop build save"で検索し，出てきたやつの選択肢から"never"を選ぶ
    - テストする
        - `helloworld.tex`を開き Ctrl + Alt + B (Win/Linux) / Cmd + Option + B (Mac)を押してPDFが生成されるかテストする
